class SimpleArray(object):

    def __init__(self, list):
        # list might be a list of lists of arbitrary dimension
        self.array = list

    def __getitem__(self, key):
        if type(key) == tuple:
            current_array = self.array
            for index_or_slice in key:
                current_array = current_array[index_or_slice]
            return current_array
        elif type(key) == int:
            return self.array[key]
