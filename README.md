The basic idea is to create something like a spreadsheet that is run from the command line.

Basically, a thin layer of abstraction above Python lists that makes things slightly easier.

Type safety? forget about it.

Example:
`
> data[1, 1]

>> 1

> data[1, :]

>> [1, 2, ...]

> data[1, :] = ['a', 'b', ...]

> data.insert_row(1, ['a', 'b', 'c', ...])

> data.insert_column(1, ...)

> data.save_csv('file')

> data.save_tsv('file')

`

More ideas:

- efficiency (B-tree indices? cython?)
