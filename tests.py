import unittest
from SimpleTable import from_list

class SimpleTableTest(unittest.TestCase):

    def setUp(self):
        self.table = from_list([[1,2,3],[4,5,6],[7,8,9]])

    def test_get(self):
        g1 = self.table[0:2, 2]
        self.assertEqual(g1, [3, 6])
        g2 = self.table[1:3, 0:2]
        self.assertEqual(g2, [[4,5],[7,8]])
        g3 = self.table[0, 1:3]
        self.assertEqual(g3, [2,3])
        g4 = self.table[1, 1]
        self.assertEqual(g4, 5)

    def test_set(self):
        table2 = from_list([[1,2,3],[4,5,6],[7,8,9]])
        table2[0, 1:3] = [0,0]
        self.assertEqual(table2.as_list(), [[1,0,0],[4,5,6],[7,8,9]])
        table2[0:2, 1] = [1,1]
        self.assertEqual(table2.as_list(), [[1,1,0],[4,1,6],[7,8,9]])
        table2[1:3, 1:3] = [[1,1], [1,1]]
        self.assertEqual(table2.as_list(), [[1,1,0],[4,1,1],[7,1,1]])
        table2[1,1] = -10
        self.assertEqual(table2.as_list(), [[1,1,0],[4,-10,1],[7,1,1]])

    def test_insert(self):
        table2 = from_list([[1,2,3],[4,5,6]])
        table2.insert_row(1, [0,0,0])
        self.assertEquals(table2.as_list(), [[1,2,3],[0,0,0],[4,5,6]])
        table2.insert_column(1, [1,1,1])
        self.assertEquals(table2.as_list(), [[1,1,2,3],[0,1,0,0],[4,1,5,6]])

    def test_delete(self):
        table2 = from_list([[1,2,3],[4,5,6],[7,8,9]])
        table2.delete_row(1)
        self.assertEquals(table2.as_list(), [[1,2,3],[7,8,9]])
        table2.delete_column(2)
        self.assertEquals(table2.as_list(), [[1,2],[7,8]])
        table2.delete_row(0)
        self.assertEquals(table2.as_list(), [[7,8]])


if __name__ == '__main__':
    unittest.main()
