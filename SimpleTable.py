import csv

class SimpleTable(object):

    def __init__(self):
        # assumption: the data is 2-dimensional
        # should the data be backed by an array or a dict?
        # using a list requires linear-time deletion, but using
        # a dict also requires linear-time key changing.
        self.data_array = []
        # row and column indices should be sorted integers
        self.row_indices = {}
        self.col_indices = {}
        self.max_index = 0
        self.max_col_index = 0

    def __getitem__(self, key):
        """
        Copies the elements
        """
        if isinstance(key, tuple):
            # ignore if tuple has more than 2 elements
            i = key[0]
            temp_array = []
            if isinstance(i, slice):
                temp_array = [list(x) for x in self.data_array[i]]
            elif isinstance(i, int):
                temp_array = list(self.data_array[i])
            i = key[1]
            final_array = []
            # whether the second key is an index or a slice,
            # this can be done.
            if isinstance(temp_array[0], list):
                for subarray in temp_array:
                    final_array.append(subarray[i])
            else:
                final_array = temp_array[i]
            return final_array
        elif isinstance(key, slice):
            return [list(x) for x in self.data_array[key]]
        elif isinstance(key, int):
            return list(self.data_array[key])
        else:
            raise KeyError('Incorrect key')

    def __setitem__(self, key, item):
        """
        """
        if isinstance(key, tuple):
            if isinstance(key[0], int) and isinstance(key[1], int):
                self.data_array[key[0]][key[1]] = item
            else:
                i = key[0]
                temp_array = []
                if isinstance(i, slice):
                    temp_array = self.data_array[i]
                elif isinstance(i, int):
                    temp_array = [self.data_array[i]]
                i = key[1]
                # whether the second key is an index or a slice,
                # this can be done.
                for index, subarray in enumerate(temp_array):
                    # if 'item' is a single list
                    if isinstance(subarray[i], list) and not isinstance(item[index], list):
                        subarray[i] = item
                    else:
                        subarray[i] = item[index]
        elif isinstance(key, slice):
            indices = key.indices(len(self.data_array))
            for i, index in enumerate(range(indices[0], indices[1], indices[2])):
                # TODO: size checking
                if not isinstance(item[i], list) or len(item[i]) != len(self.data_array[i]):
                    raise KeyError('Incompatible size')
                self.data_array[index] = item[i]
        elif isinstance(key, int):
            if len(self.data_array) > 0 and len(item) == len(self.data_array[0]):
                self.data_array[key] = item
        else:
            raise KeyError('Incorrect key')

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        string = '\n'.join('['+','.join(str(x) for x in self[i, 0:10])+']' for i in range(min(len(self.data_array), 10)))
        return '[' + string + ']'

    def __len__(self):
        return len(self.data_array)

    def size(self):
        """
        Returns the dimensions of the matrix - (nrows, ncols)
        """
        if len(self.data_array) > 0:
            return (len(self.data_array), len(self.data_array[0]))
        else:
            return (0, 0)

    def insert_row(self, index, row, truncate=True):
        if len(self.data_array) == 0 or truncate == False:
            self.data_array.insert(index, row)
        elif len(row) > len(self.data_array[0]):
            print 'Warning: truncating row'
            self.data_array.insert(index, row[:len(self.data_array[0])])
        elif len(row) < len(self.data_array[0]):
            print 'Warning: extending row with blanks'
            self.data_array.insert(index, row + ['' for i in range(len(row), len(self.data_array[0]))])
        else:
            self.data_array.insert(index, row)

    def insert_column(self, index, column):
        """
        Inserts a column into the specified index.
        If the column is larger than the matrix, it is truncated.
        If it is smaller than the matrix, it is extended with empty characters.
        """
        if len(column) > len(self.data_array):
            print 'Warning: truncating column'
        elif len(column) < len(self.data_array):
            print 'Warning: extending column with blanks'
        for i, row in enumerate(self.data_array):
            if i < len(column):
                row.insert(index, column[i])
            else:
                row.insert(index, '')

    def delete_row(self, index):
        # have to move all the indices down by 1...
        # this should be linear in the number of keys
        del self.data_array[index]
        self.max_index -= 1

    def delete_column(self, index):
        for row in self.data_array:
            del row[index]
        self.max_col_index -= 1

    def append_row(self, row, truncate = True):
        """
        Unless the data array is initially empty, the row is
        truncated.
        """
        if len(self.data_array) == 0 or truncate == False:
            self.data_array.append(row[:])
        else:
            if len(self.data_array[0]) > len(row):
                print 'Warning: extending row with blanks'
                self.data_array.append(row + ['' for i in range(len(row), len(self.data_array[0]))])
            elif len(self.data_array[0]) < len(row):
                print 'Warning: truncating row'
                self.data_array.append(row[:len(self.data_array[0])])
            else:
                self.data_array.append(list(row))
        self.max_index += 1

    def append_column(self, column):
        """
        Appends a column to the table.
        The column is truncated if it is longer than the number
        of rows. If it is shorter than the number of rows,
        the remaining positions are filled with blanks.
        """
        if len(column) > len(self.data_array):
            print 'Warning: truncating column'
        elif len(column) < len(self.data_array):
            print 'Warning: extending column with blanks'
        for i, row in enumerate(self.data_array):
            if i < len(column):
                row.append(column[i])
            else:
                row.append('')
        self.max_col_index += 1

    def save_csv(self, filename):
        with open(filename, 'w') as open_file:
            writer = csv.writer(open_file, delimiter = ',')
            for row in self.data_array:
                writer.writerow(row)

    def save_tsv(self, filename):
        with open(filename, 'w') as open_file:
            writer = csv.writer(open_file, delimiter = '\t')
            for row in self.data_array:
                writer.writerow(row)

    def as_list(self):
        return list(list(row) for row in self.data_array)

def read_csv(csv_file, delimiter = ',', truncate=True):
    """
    Reads a SimpleTable from a CSV file.
    """
    table = SimpleTable()
    with open(csv_file) as open_file:
        for line in csv.reader(open_file, delimiter = delimiter,
                skipinitialspace = True):
            if line:
                table.append_row(line, truncate)
    return table

def from_list(list_of_lists, truncate=True):
    """
    Creates a SimpleTable from a list of lists.
    """
    table = SimpleTable()
    for row in list_of_lists:
        table.append_row(row, truncate)
    return table
